var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var User = mongoose.model('User');
var Course = mongoose.model('Course');
var Instructor = mongoose.model('Instructor');
var Assignment = mongoose.model('Assignment');
var Request = mongoose.model('Request');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Dashboard' });
});

// A route for the 'restricted' section (the dashboard)
router.get('/dashboard', function(req, res, next) {
    //res.render('dashboard', { title: 'Dashboard' });
    if(req.session.user) {
        // valid user in the session, continue to the dashboard
        res.render('dashboard', { title: 'Dashboard', username: req.session.user.name});
    }
    else {
        req.session.error = 'Access Denied!';
        res.redirect('/login');
    }
});

router.get('/courses', function(req, res, next) {
	Course.find(function(err,Courses){
		if(err){return next(err);}
		res.json(Courses);
	})
	//res.send("Courses rest service here");
});

router.post('/courses', function(req, res, next){
	console.log(req.body);
	console.log(req.post);
	var newCourse = new Course(req.body);
	newCourse.save(function(err, newCourse){
		if(err){return next(err);}
		res.json(newCourse);
	});
});

router.param('course',function(req, res, next, id){
	var query = Course.findById(id);
	query.exec(function(err, course){
		if(err){return next(err);}
		if(!course) {return next (new Error("can't find post"));}
		req.course = course;
		return next();
	});
});

router.get('/courses/:course', function(req, res){
	res.json(req.course);
});

router.post('/courses/:course/teachers', function(req, res, next){
	var teacher = new Instructor(req.body);
	teacher.save(function(err, teacher){
		req.course.instructors.push(teacher);
		req.course.save(function(err,course){
			if(err){return next(err);}
			res.json(course);
		});
	});
});

router.get('/courses/:course/teachers',function(req,res,next){
	req.course.populate('instructors', function(err, course){
		res.json(course.instructors);
	});
});

router.get('/teachers', function(req, res, next) {
	Instructor.find(function(err,instructors){
		if(err){return next(err);}
		res.json(instructors);
	});
	//res.send("Courses rest service here");
});

router.post('/teachers', function(req, res, next){
	console.log(req.body);
	var newInstructor = new Instructor(req.body);
	newInstructor.save(function(err, newInstructor){
		if(err){return next(err);}
		res.json(newInstructor);
	});
});

router.param('teacher',function(req, res, next, id){
	console.log("in teacher param");
	var query = Instructor.findById(id);
	query.exec(function(err, teacher){
		if(err){return next(err);}
		if(!teacher) {return next (new Error("can't find teacher"));}
		req.teacher = teacher;
		return next();
	});
});

router.get("/teachers/:teacher", function(req, res){
	res.json(req.teacher);
});

router.get("/teachers/:teacher/assignments",function(req,res,next){
	req.teacher.populate('assignments', function(err, teacher){
		res.json(teacher.assignments);
	});
});
	
router.post("/teachers/:teacher/assignments",function(req, res, next){
	var assignment = new Assignment(req.body);
	assignment.save(function(err, teacher){
		req.teacher.assignments.push(assignment);
		req.teacher.save(function(err,teacher){
			if(err){return next(err);}
			res.json(teacher);
		});
	});
});

router.get('/assignments', function(req, res, next){
	Assignment.find(function(err, assignments){
		if(err){return next(err);}
		res.json(assignments);
	});
});

router.post('/assignments', function(req, res, next){
	console.log(req.body);
	var assignment = new Assignment(req.body);
	assignment.save(function(err, assignment){
		if(err){return next(err);}
		res.json(assigment);
	});
});

router.param('assignment',function(req, res, next, id){
	console.log("in assignment param");
	var query = Assignment.findById(id);
	query.exec(function(err, assignment){
		if(err){return next(err);}
		if(!assignment) {return next (new Error("can't find assigment"));}
		req.assignment = assignment;
		return next();
	});
});

router.get("/assignments/:assignment", function(req, res){
	res.json(req.assignment);
});

router.get("/assignments/:assignment/problems",function(req,res,next){
	res.json(req.assignment.problems);
});

router.post("/assignments/:assignment/problems",function(req, res, next){
	console.log("in problems post");
	console.log(req.body);
	var problem = req.body;
	console.log("problem: " + problem.title);
	console.log("assingment: " + req.assignment);
	req.assignment.problems.push(problem.title);
	console.log("pushed");
	req.assignment.save(function(err,assignment){
		if(err){return next(err);}
		res.json(assignment);
	});
});

router.post('/request', function(req, res, next){
	console.log("adding request");
	var request = new Request(req.body);
	request.save(function(err,request){
		if(err){return next(err);}
		res.json(request);
	});
});

router.get('/requests', function(req,res){
	Request.find()
	.populate('course instructor assignment')
	.exec(function(err, requests){
		res.json(requests);
	});
});

router.post('/removeRequest', function(req,res) {
	console.log("in removeRequest");
	console.log(req.body._id);
	var request = req.body;
	Request.remove({_id:req.body._id}, function(err,request) {
		if(err){return next(err);}
		res.json(request);
	}) 
});

router.param('request',function(req, res, next, id){
	console.log("in request param");
	var query = Request.findById(id);
	query.exec(function(err, request){
		if(err){return next(err);}
		if(!Request) {return next (new Error("can't find Request"));}
		req.request = request;
		return next();
	});
});

router.post('/request/getNext', function(req, res, next){
	console.log("TA Gets Request");
	console.log(req.body);
	var info = req.body;
	console.log("TA: " + req.body);
	Request.findOne({'status': 'open' }).populate('course instructor assignment').exec(function(err,request) {
		if (err) return handleError(err);
		if (request) {
			request.start();
			console.log(request);		
			request.save(function(err,requestt){
				if(err){console.log(err); return next(err);}
				res.json(requestt);
			});
		}		
	});
	console.log("Getting Help");
});

router.post('/request/:request/close', function(req, res, next){
	console.log("TA closing Request");
	console.log(req.body);
	var info = req.body;
	console.log("Request: " + req.request);
	req.request.end();
	console.log("Closed");
	req.request.save(function(err,assignment){
		if(err){return next(err);}
		res.json(assignment);
	});
});

router.get('/login', function(req, res, next){
    if(req.session.user){
        res.redirect('/dashboard');
    }
    else{
        res.render('login', { title: 'Login', message: req.session.message});
    }
});

router.post('/login', function(req, res){
    // user should be a lookup of req.body.userEmail in database -- TODO
    var user = {name:req.body.userEmail, pass:"password"};
    
    // regenerate the session
    req.session.regenerate(function(err){});
    
    // If the login is valid, then set the session objects
    if(req.body.userPassword === user.pass){
        req.session.user = user;
        req.session.message = 'Authenticated as ' + user.name;
        res.message = "Welcome "+user.name+"!";
    }
    else{
        req.session.message = "Invalid Login!";
        res.message = "Username or password is incorrect";
    }
    
    res.redirect('/login');
});

router.get('/logout', function(req, res){
    req.session.destroy(function(){
        // destroy the session and redirect to home
        res.redirect('/');
    });
});

module.exports = router;
