var mongoose = require('mongoose');
var RequestSchema = new mongoose.Schema({
  status: {type: String, default: "open"},
  name: String,
  requestDate: {type: Date, default: Date.now},
  course:  { type: mongoose.Schema.Types.ObjectId, ref: 'Course' },
  instructor: { type: mongoose.Schema.Types.ObjectId, ref: 'Instructor' },
  assignment: { type: mongoose.Schema.Types.ObjectId, ref: 'Assignment' },
  problem: String,//{ type: String },
  notes: String,
  ta: String,
  startHelp: Date,
  endHelp: Date,
  resolution: String,
});
RequestSchema.methods.start = function(cb) {
  this.startHelp = Date.now();
  this.ta = "DEMO";
  this.status = "Getting Help";
  this.save(cb);
};
RequestSchema.methods.end = function(cb) {
  this.endHelp = Date.now();
  this.status = "closed";
  this.save(cb);
};
mongoose.model('Request', RequestSchema);