var mongoose = require('mongoose');
var AssignmentSchema = new mongoose.Schema({
  title: String,
  problems: [String]
});
mongoose.model('Assignment', AssignmentSchema);