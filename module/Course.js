var mongoose = require('mongoose');
var CourseSchema = new mongoose.Schema({
  name:  String,
  instructors: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Instructor' }]
});
mongoose.model('Course', CourseSchema);