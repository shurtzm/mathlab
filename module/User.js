var mongoose = require('mongoose');

var UserSchema = new mongoose.Schema({
  username: { type: String, required: true, index: { unique: true } },
  password: { type: String, required: true },
  firstName: String,
  lastName: String,
  email: String,
  isTA: Boolean,
  isAdmin: Boolean,
  notes: String
});

mongoose.model('User', UserSchema);