var mongoose = require('mongoose');
var InstructorSchema = new mongoose.Schema({
  firstName: String,
  lastName: String,
  assignments: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Assignment' }]
});
mongoose.model('Instructor', InstructorSchema);