angular.module('mathLab', ['ui.router'])
.factory('assignmentFactory', ['$http', function($http){
  var o = {
    assignments: [
    	'Junk 1',
    	'Junk 2',
    	'IT WORKS!'
    	],
    assignment: {}
  };
  o.getAll = function() {
    return $http.get('/assignments').success(function(data){
      angular.copy(data, o.assignments);
    });
  };
  return o;
}])