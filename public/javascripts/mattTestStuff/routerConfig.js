angular.module('mathLab', ['ui.router'])
.config([
  '$stateProvider',
  '$urlRouterProvider',
  function($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('requests', {
        url: '/requests',
        templateUrl: '/requests.html',
        controller: 'RequestPage'
      })
      .state('closedRequests', {
        url: '/closed',
        templateUrl: '/closed.html',
        controller: 'RequestPage'
      })
      .state('users', {
        url: '/users',
        templateUrl: '/users.html',
        controller: 'RequestPage'
      })
      .state('courses', {
        url: '/courses',
        templateUrl: '/courses.html',
        controller: 'ManageCoursesPage'
      })
      .state('teachers', {
        url: '/teachers',
        templateUrl: '/teachers.html',
        controller: 'ManageTeachersPage'
      });
    $urlRouterProvider.otherwise('requests');
}])