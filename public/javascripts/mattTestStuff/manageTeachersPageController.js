angular.module('mathLab', ['ui.router'])
.controller('ManageTeachersPage', [
  '$scope',
  'courseFactory',
  'teacherFactory',
  'assignmentFactory',
  'problemFactory',
  function ($scope, courseFactory, teacherFactory, assignmentFactory, problemFactory) {
    $scope.addTeacher = function(){
      if($scope.firstName === '') { return; }
      teacherFactory.create({
        firstName: $scope.firstName,
        lastName: $scope.lastName,
        assignments:[]
      });
      console.log($scope.course);
      console.log("PLEASE: " + teacherFactory.teacher.lastName);
      courseFactory.addTeacher(teacherFactory.teacher,$scope.course);
         

    };
    courseFactory.getAll();
    $scope.courses = courseFactory.courses;
  }]);
