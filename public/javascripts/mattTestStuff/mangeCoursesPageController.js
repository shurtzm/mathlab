angular.module('mathLab', ['ui.router'])
.controller('ManageCoursesPage', [
  '$scope',
  'courseFactory',
  'teacherFactory',
  'assignmentFactory',
  'problemFactory',
  function ($scope, courseFactory, teacherFactory, assignmentFactory, problemFactory) {
    courseFactory.getAll();
    $scope.courses = courseFactory.courses;
    $scope.addCourse = function(){
      if($scope.name === '') { return; }
      courseFactory.create({
        name: $scope.name,
        instructors:[]
      });
      $scope.name = '';
    };
  }])