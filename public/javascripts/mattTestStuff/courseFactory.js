angular.module('mathLab')
.factory('courseFactory', ['$http', function($http){
  var o = {
    courses: [
    	'Junk 1',
    	'Junk 2',
    	'IT WORKS!'
    	],
    course: {}
  };
  o.getAll = function() {
    return $http.get('/courses').success(function(data){
      angular.copy(data, o.courses);
    });
  };
  o.create = function(cl) {
    console.log("o.create");
    console.log(cl);
    return $http.post('/courses', cl).success(function(data){
      o.courses.push(data);
    });
  };
  o.addTeacher = function(teacher, c) {
    console.log(teacher);
    return $http.post('/courses/'+ c._id +'/teachers',teacher).success(function(){
      o.getAll();
    });
  }
  return o;
}])