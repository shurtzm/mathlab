angular.module('mathLab', ['ui.router'])
.factory('problemFactory', ['$http', function($http){
  var o = {
    problems: [
    	'Junk 1',
    	'Junk 2',
    	'IT WORKS!'
    	],
    problem: {}
  };
  o.getAll = function() {
    return $http.get('/problems').success(function(data){
      angular.copy(data, o.problems);
    });
  };
  return o;
}])