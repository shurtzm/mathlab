angular.module('mathLab', ['ui.router'])
.controller('RequestPage', [
  '$scope',
  'courseFactory',
  'teacherFactory',
  'assignmentFactory',
  'problemFactory',
  function ($scope, courseFactory, teacherFactory, assignmentFactory, problemFactory) {
    courseFactory.getAll();
    $scope.courses = courseFactory.courses;
    
    
    assignmentFactory.getAll();
    $scope.assignments = assignmentFactory.teachers;

    $scope.changeCourse = function() {
        console.log("SELECTED course: " + $scope.selectedCourse);
        teacherFactory.getAll();
        $scope.teachers = teacherFactory.teachers;
    }

    $scope.changeTeacher = function() {
        console.log("SELECTED Teacher: " + $scope.selectedTeacher);
        assignmentFactory.getAll();
        $scope.assignments = assignmentFactory.teachers;
    }

    $scope.changeAssignment = function() {
        console.log("SELECTED course: " + $scope.selectedTeacher);
        assignmentFactory.getAll();
        $scope.assignments = assignmentFactory.teachers;
    }


  }])