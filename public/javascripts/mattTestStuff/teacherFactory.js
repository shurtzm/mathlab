angular.module('mathLab')
.factory('teacherFactory', ['$http', function($http){
  var o = {
    teachers: [
    	'Junk 1',
    	'Junk 2',
    	'IT WORKS!'
    	],
    teacher: {}
  };
  o.getAll = function() {
    return $http.get('/teachers').success(function(data){
      angular.copy(data, o.teachers);
    });
  };
  o.create = function(info) {
    console.log("o.create");
    console.log(info);
    return $http.post('/teachers', info).success(function(data){
      o.teachers.push(data);
      angular.copy(data,o.teacher);
      console.log(o.teacher.lastName);
    });
  }
  return o;
}])