angular.module('mathLab', ['ui.router'])
.factory('courseFactory', ['$http', function($http){
  var o = {
    courses: [
    	'Junk 1',
    	'Junk 2',
    	'IT WORKS!'
    	],
    course: {}
  };
  o.getAll = function() {
    return $http.get('/courses').success(function(data){
      angular.copy(data, o.courses);
    });
  };
  o.create = function(cl) {
    console.log("o.create");
    console.log(cl);
    return $http.post('/courses', cl).success(function(data){
      o.courses.push(data);
    });
  };
  o.addTeacher = function(fName, lName, c) {
    console.log(fName);
    console.log(lName);
    console.log(c);
    var teacher = {firstName: fName,
    		lastName: lName,
    		assignments:[]
    }
    return $http.post('/courses/'+ c._id +'/teachers',teacher).success(function(){
      o.getAll();
    });
  }
  return o;
}])

.factory('teacherFactory', ['$http', function($http){
  var o = {
    teachers: [],
    teacher: {}
  };
  o.getByCourse = function(course) {
	  teachers = [];
	  teacher = {};
	  console.log(course._id);
	  return $http.get('/courses/' + course._id + '/teachers').success(function(data) {
		  angular.copy(data, o.teachers);
		  console.log(o.teachers);
	  });
  };
  o.getAll = function() {
    return $http.get('/teachers').success(function(data){
      angular.copy(data, o.teachers);
    });
  };
  
  o.addAssignment = function(name,teacher) {
	  var assignment = {
			    title: name,
	    		problems:[]
	    }
	  console.log("name: " + name);
	  console.log("teacher: " + teacher);
	    return $http.post('/teachers/'+ teacher._id +'/assignments',assignment).success(function(){
	      o.getAll();
	    });
  };
  return o;
}])

.factory('assignmentFactory', ['$http', function($http){
  var o = {
    assignments: [
    	'Junk 1',
    	'Junk 2',
    	'IT WORKS!'
    	],
    assignment: {}
  };
  o.getAll = function() {
    return $http.get('/assignments').success(function(data){
      angular.copy(data, o.assignments);
    });
  };
  o.getByTeacher = function(teacher) {
	  console.log(teacher);
	  return $http.get('/teachers/' + teacher._id + '/assignments').success(function(data) {
		  angular.copy(data, o.assignments);
	  });
  };
  o.addProblem = function(name,assignment) {
	  var problem = {
			    title: name
	    }
	  console.log("name: " + name);
	  console.log("teacher: " + teacher);
	    return $http.post('/assignments/'+ assignment._id +'/problems',problem).success(function(){
	      o.getAll();
	    });
  };
  return o;
}])

.factory('problemFactory', ['$http', function($http){
  var o = {
    problems: [
    	'Junk 1',
    	'Junk 2',
    	'IT WORKS!'
    	],
    problem: {}
  };
  o.getAll = function() {
    return $http.get('/problems').success(function(data){
      angular.copy(data, o.problems);
    });
  };
  o.getByAssignment = function(assignment) {
	  console.log(assignment);
	  return $http.get('/assignments/' + assignment._id + '/problems').success(function(data) {
		  angular.copy(data, o.problems);
	  });
  };
  return o;
}])

.factory('loginFactory', ['$http', function($http){
    var o = {
        userEmail: {},
        userPassword: {}
    };
    
    o.login = function(email, pass) {
        o.userEmail = email;
        o.userPassword = pass;
        return $http.post('/login', o)
        .success(function(data, status, headers, config) {
            console.log("Success!");
        })
        .error(function(data, status, headers, config) {
            console.log("Error!");
        });
    }
    return o;
}])

.factory('requestFactory', ['$http', function($http){
	var o = {
		requests: [],
		request: {'name':"Queue is Empty"}
	};
	
	o.addRequest = function(requestInfo){
		return $http.post('/request', requestInfo)
		.success(function(data){
			o.request = data;
		});
	}
	
	o.getAll = function(){
		return $http.get('/requests').success(function(data){
			angular.copy(data, o.requests);
		});
	}

  o.removeRequest = function(request) {
		return $http.post('/removeRequest', request)
		.success(function(data){
			angular.copy(data, o.requests);
		});
	}

  o.getNextRequest = function() {
    if(o.request.name=="Queue is Empty"){
          return $http.post('/request/getNext').success(function(data){
            angular.copy(data, o.request);
            o.getAll();
          });
    }

  }

  o.finnished = function() {
    console.log("TA is finnished");
    return $http.post('/request/'+ o.request._id + '/close').success(function(data){
      o.getAll();
      o.request = {'name':"Queue is Empty"};
    });
  }

	return o;
}])

.config([
  '$stateProvider',
  '$urlRouterProvider',
  function($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('requests', {
        url: '/requests',
        templateUrl: '/requests.html',
        controller: 'ManageRequestPage'
      })
      .state('closedRequests', {
        url: '/closed',
        templateUrl: '/closed.html',
        controller: 'ManageRequestPage'
      })
      .state('users', {
        url: '/users',
        templateUrl: '/users.html',
        controller: 'RequestPage'
      })
      .state('courses', {
        url: '/courses',
        templateUrl: '/courses.html',
        controller: 'ManageCoursesPage'
      })
      .state('teachers', {
        url: '/teachers',
        templateUrl: '/teachers.html',
        controller: 'ManageTeachersPage'
      })
      .state('assignments', {
        url: '/assignments',
        templateUrl: '/assignments.html',
        controller: 'ManageAssignmentsPage'
      })
      .state('problems', {
        url: '/problems',
        templateUrl: '/problems.html',
        controller: 'ManageProblemsPage'
      });
    $urlRouterProvider.otherwise('requests');
}])

.controller('RequestPage', [
  '$scope',
  'courseFactory',
  'teacherFactory',
  'assignmentFactory',
  'problemFactory',
  'requestFactory',
  function ($scope, courseFactory, teacherFactory, assignmentFactory, problemFactory, requestFactory, ngTableParams) {
    courseFactory.getAll();
    $scope.courses = courseFactory.courses;
    requestFactory.getAll();
    $scope.requests = requestFactory.requests;
    $scope.changeCourse = function() {
        console.log("SELECTED course: " + $scope.selectedCourse.name);
        teacherFactory.getByCourse($scope.selectedCourse);
        console.log(teacherFactory.teachers);
        $scope.teachers = teacherFactory.teachers;
        $scope.selectedTeacher = teacher;
    }

    $scope.changeTeacher = function() {
        console.log("SELECTED Teacher: " + $scope.selectedTeacher);
        assignmentFactory.getByTeacher($scope.selectedTeacher);
        $scope.assignments = assignmentFactory.assignments;
    }

    $scope.changeAssignment = function() {
        console.log("SELECTED assignment: " + $scope.selectedAssignment);
        problemFactory.getByAssignment($scope.selectedAssignment);
        $scope.problems = problemFactory.problems;
    }
    
    $scope.submitRequest = function() {
    	console.log("Submitting request");
    	requestFactory.addRequest(
			{
				name: $scope.name,
				course: $scope.selectedCourse,
				instructor: $scope.selectedTeacher,
				assignment: $scope.selectedAssignment,
				problem: $scope.selectedProblem
			}
    	);
    	requestFactory.getAll();
    	$scope.requests = requestFactory.requests;
    };

  }])

.controller('ManageRequestPage', [
  '$scope',
  'courseFactory',
  'teacherFactory',
  'assignmentFactory',
  'problemFactory',
  'requestFactory',
  'loginFactory',
  function ($scope, courseFactory, teacherFactory, assignmentFactory, problemFactory, requestFactory, ngTableParams,loginFactory) {
    courseFactory.getAll();
    $scope.courses = courseFactory.courses;
    requestFactory.getAll();
    $scope.requests = requestFactory.requests;

    $scope.getNextRequest = function() {
        console.log("TA: " + $scope.iser);
        //console.log(loginFactory.userEmail);
        requestFactory.getNextRequest("DEMO");
        console.log(requestFactory.request);
        requestFactory.getAll();
    }
    $scope.request = requestFactory.request;

    $scope.showPending = function (item) {
      return item.status === "open";
    };

    $scope.showClosed = function (item) {
      return (item.status === "closed" || item.status === "Getting Help");
    };

    $scope.finnishRequest = function() {
      console.log("Finnished Request");
      requestFactory.finnished();
    }

    $scope.removeRequest = function(request) {
      console.log("in removeREquest");
      console.log(request);
      requestFactory.removeRequest(request);
      requestFactory.getAll();
      $scope.requests = requestFactory.requests;
    };

  }])
  
.controller('ManageCoursesPage', [
  '$scope',
  'courseFactory',
  'teacherFactory',
  'assignmentFactory',
  'problemFactory',
  function ($scope, courseFactory, teacherFactory, assignmentFactory, problemFactory) {
    courseFactory.getAll();
    $scope.courses = courseFactory.courses;
    $scope.addCourse = function(){
      if($scope.name === '') { return; }
      courseFactory.create({
        name: $scope.name,
        instructors:[]
      });
      $scope.name = '';
    };
  }])

.controller('ManageTeachersPage', [
  '$scope',
  'courseFactory',
  'teacherFactory',
  'assignmentFactory',
  'problemFactory',
  function ($scope, courseFactory, teacherFactory, assignmentFactory, problemFactory) {
    $scope.addTeacher = function(){
      if($scope.firstName === '') { return; }
//      teacherFactory.create({
//        firstName: $scope.firstName,
//        lastName: $scope.lastName,
//        assignments:[]
//      });
//      console.log($scope.course);
//      console.log("PLEASE: " + teacherFactory.teacher.lastName);
      courseFactory.addTeacher($scope.firstName,$scope.lastName,$scope.course);
      //teacherFactory.teacher
    };
    courseFactory.getAll();
    $scope.courses = courseFactory.courses;
  }])
  
  .controller('ManageAssignmentsPage', [
  '$scope',
  'courseFactory',
  'teacherFactory',
  'assignmentFactory',
  'problemFactory',
  function ($scope, courseFactory, teacherFactory, assignmentFactory, problemFactory) {
    courseFactory.getAll();
    $scope.courses = courseFactory.courses;
    $scope.course = courseFactory.course;
    $scope.changeCourse = function() {
        console.log("SELECTED course: " + $scope.selectedCourse.name);
        teacherFactory.getByCourse($scope.selectedCourse);
        console.log(teacherFactory.teachers);
        $scope.teachers = teacherFactory.teachers;
        $scope.selectedTeacher = teacher;
    }
    $scope.addAssignment = function(){
    	console.log('in add assignment');
    	console.log($scope.selectedTeacher);
      teacherFactory.addAssignment($scope.name, $scope.selectedTeacher);
      $scope.name = '';
    };
  }])
  
  .controller('ManageProblemsPage', [
  '$scope',
  'courseFactory',
  'teacherFactory',
  'assignmentFactory',
  'problemFactory',
  function ($scope, courseFactory, teacherFactory, assignmentFactory, problemFactory) {
    courseFactory.getAll();
    $scope.courses = courseFactory.courses;
    $scope.changeCourse = function() {
        console.log("SELECTED course: " + $scope.selectedCourse.name);
        teacherFactory.getByCourse($scope.selectedCourse);
        console.log(teacherFactory.teachers);
        $scope.teachers = teacherFactory.teachers;
        $scope.selectedTeacher = teacher;
    }
    $scope.changeTeacher = function() {
        console.log("SELECTED course: " + $scope.selectedCourse.name);
        assignmentFactory.getByTeacher($scope.selectedTeacher);
        console.log(teacherFactory.teachers);
        $scope.assignments = assignmentFactory.assignments;
    }
    $scope.addProblem = function(){
    	console.log('in add problem');
    	console.log($scope.selectedAssignment);
      assignmentFactory.addProblem($scope.problem, $scope.selectedAssignment);
      $scope.name = '';
    };
  }])

  .controller('loginCtrl', [
      '$scope',
      '$window',
      '$http',
      'loginFactory',
      function($scope, $window, $http, loginFactory){
          $scope.login = function() {
              loginFactory.login($scope.userEmail, $scope.userPassword);
              //$window.alert($http.data);
              $window.location.href = "/dashboard";
          };
      }
  ]);
